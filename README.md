# Dot files
The following dependencies are required (code given for Debian and Arch-based distros)

## Qtile

1. Compton (for transparency)
```bash
sudo apt install compton
```

or on Arch:
```bash
sudo pacman -S picom
```

2. xwallpaper
```bash
sudo apt install xwallpaper
```

or on Arch:
```bash
sudo pacman -S xwallpaper
```

3. Lightdm and light-locker (for login manager and locking laptop)
```bash
sudo apt install lightdm light-locker
```

or on Arch:
```bash
sudo pacman -S lightdm light-locker
```

4. Dunst (for notifications)
```bash
sudo apt install dunst
```

or on Arch:
```bash
sudo pacman -S dunst
```

5. Network Manager Applet
(Not confirmed for Debian-based)
```bash
sudo apt install nm-applet
```

or on Arch:
```bash
sudo pacman -S network-manager-applet
```

6. Volume Icon
```bash
sudo apt install volumeicon-alsa
```

or on Arch:
```bash
sudo pacman -S volumeicon
```

7. Qtile dependencies for running
```bash 
pip install 'xcffib >= 0.10.1' && pip install --no-cache-dir 'cairocffi >= 0.9.0'
sudo apt install mypy
```

Run the following commands to install Qtile:
```bash
git clone git://github.com/qtile/qtile.git
cd qtile
pip install
```
and then clone this repository and copy the dotfiles into your `.config`. 

To make sure everything works, run `qtile check` in your terminal. 

On Arch, simply run:

```bash
sudo pacman -S qtile mypy
```

## Neovim (Vim)
You need to install vim-plug in order for my neovim config to work. You can install it using the following command:
```bash
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
```
Then simply open neovim and run `PlugInstall`.

You might also want to run `:CheckHealth` to see if neovim is configured properly. You might have to install some more packages for it to work. 

You also need Zathura to preview the pdf documents. You can install it with the following command:
```bash
sudo apt install zathura
```

or on Arch:
```bash
sudo pacman -S neovim zathura zathura-pdf-poppler
```

## Alacritty

1. Nerd font

I installed the Sauce Code Pro Nerd Font. It is required for my alacritty configuration to work. You can download it with [this link](https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/SourceCodePro.zip) or by using this link 'https://www.nerdfonts.com/font-downloads'. When you download the zip file, unzip it and copy the folder into `~/.local/share/fonts/`.
If the folder doesn't exist, create it witzathura-pdf-poppler
```
You can check to make sure the font is installed with 
```bash
fc-list | grep "Sauce Code Pro"
```
If nothing shows up, then the font was not installed properly. Otherwise, we can proceed to the second dependency.

2. Starship prompt
Run the following command to download the Starship prompt.
```bash
sh -c "$(curl -fsSL https://starship.rs/install.sh)"
```
You must add `eval "$(starship init bash)"` to the end of your `.bashrc` for the prompt to work. Then, simply copy my `starship.toml` into your `.config` folder. 

## Bash
I use exa instead of the traditional ls. To install exa, run the commands:
```bash
sudo apt install cargo
sudo cargo install exa
```

or on Arch:
```bash
sudo pacman -S exa
```

In your `.bashrc`, change the 'ls' alias to:
```bash
alias ls='exa -al --color=always --group-directories-first'
```

We also need macchina. It can be installed on Debian-based using:
```bash
cargo install macchina
```

or for Arch-based:
```bash
yay -S macchina
```

## Misc
I use autorandr to configure my display. You can install autorandr using the following command:

```bash
sudo apt install autorandr
```

or on Arch:
```bash
sudo pacman -S autorandr
```
